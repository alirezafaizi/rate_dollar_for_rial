import requests
from bs4 import BeautifulSoup


def get_dollar_rate():
    url = 'https://www.tgju.org/%D9%82%DB%8C%D9%85%D8%AA' \
          '-%D8%AF%D9%84%D8%A7%D8%B1'
    response_get = requests.get(url)
    if response_get:
        print('connection was successful')
        return beautiful(response_get.text)
    else:
        print('connection was unsuccessful')


def beautiful(doc_html):
    soup = BeautifulSoup(doc_html, 'html.parser')
    rate_usd = soup.select_one(
        '#main > div > div.article-scope-body > div:nth-child(1) > div > div'
        ' > div:nth-child(1) > table > tbody > tr:nth-child(1) >'
        ' td:nth-child(2)')
    return rate_usd.text
