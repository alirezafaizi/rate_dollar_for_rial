from setuptools import setup

setup(name='price_dollar_for_rial',
      version='0.1',
      url='',
      author='alireza faizi',
      license='MIT',
      package=['rate'],
      install_requires=['requests', 'beautifulsoup4'],
      zip_safe=False)
